package anime.art.puzzles;

import android.content.DialogInterface;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;

import java.util.ArrayList;

import anime.art.puzzles.fragments.PuzzleFragment;
import anime.art.puzzles.puzzle.PuzzleSurface;


/**
 * A set of tests for the puzzle application to make sure that all puzzles will load and that they will also resize using the menu.
 */
public class ApplicationTest extends ActivityInstrumentationTestCase2<MainActivity> {

    MainActivity mainActivity;
    PuzzleFragment puzzleFragment;
    PuzzleSurface puzzleSurface;
    Menu menu;
    Data data;

    public ApplicationTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mainActivity = getActivity();
        puzzleFragment = (PuzzleFragment) mainActivity.getSupportFragmentManager().findFragmentByTag("puzzleFragmentTag");
        puzzleSurface = puzzleFragment.puzzleSurface;
        menu = puzzleFragment.menu;
        data = new Data();
    }

    public void testMainActivityNotNull() {
        assertNotNull("MainActivity is null", mainActivity);
    }

    public void testPuzzleFragmentNotNull() {
        assertNotNull("PuzzleFragment is null", puzzleFragment);
    }

    public void testPuzzleSurfaceNotNull() {
        assertNotNull("PuzzleSurface is null", puzzleSurface);
    }

    public void testMenuNotNull() {
        assertNotNull("Menu is null", menu);
    }

    /**
     * Tests both the recreate functionality of using the menu button and the resize functionality by recreating the puzzle and getting a new re sized puzzle.
     */
    public void testMenuPuzzleRecreate() {
        int imageCount = data.PICS * 3;
        ArrayList<Integer> usedInts = new ArrayList<>();

        for (int i = 0; i < imageCount; i++) {
            Log.d("puzzleDebug", "test" + i);

            if(puzzleSurface.common.currentPuzzleImagePosition == 137){
                int x = 0;
                x++;
            }

            //set the new puzzle size to be from 2 - 7
            puzzleSurface.defaultPuzzleSize = "" + ((i % 6) + 2);
            usedInts.add(puzzleSurface.common.currentPuzzleImagePosition);

            getInstrumentation().sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);

            getInstrumentation().runOnMainSync(new Runnable() {
                @Override
                public void run() {
                    menu.performIdentifierAction(
                            R.id.new_puzzle, 0);
                }
            });

            getInstrumentation().waitForIdleSync();

            getInstrumentation().runOnMainSync(new Runnable() {
                @Override
                public void run() {
                    puzzleSurface.dialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
                }
            });

            getInstrumentation().waitForIdleSync();

            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < data.PICS; i++) {
            assertTrue(usedInts.contains(i));
        }
    }

    public void testMenuPuzzleRecreateAndCloseCleanly() {
        getInstrumentation().sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);

        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                menu.performIdentifierAction(
                        R.id.new_puzzle, 0);
            }
        });

        getInstrumentation().waitForIdleSync();

        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                puzzleSurface.dialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
            }
        });

        getInstrumentation().waitForIdleSync();

        mainActivity.finish();
    }
}