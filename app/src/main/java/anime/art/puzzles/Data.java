package anime.art.puzzles;

/**
 * A class to hold the static data used across the application.
 *
 * @author Rick
 */
public class Data {
    public static int TRACK_01 = R.raw.track;
    public final int PICS = 142;
    public Artwork[] artworks;

    public Data() {
        artworks = new Artwork[PICS];
        artworks[0] = new Artwork("Yuki_Nagato", "HitsuHere", "http://hitsuhere.deviantart.com/", R.drawable.image00);
        artworks[1] = new Artwork("Adventure_Time", "HitsuHere", "http://hitsuhere.deviantart.com/", R.drawable.image01);
        artworks[2] = new Artwork("Stocking_D_City_Rock", "HitsuHere", "http://hitsuhere.deviantart.com/", R.drawable.image02);
        //
        artworks[3] = new Artwork("Spring_Forest", "Even_Skaranger_Art", "http://evenskarangerart.deviantart.com/", R.drawable.image03);
        artworks[4] = new Artwork("Madikken_in_the_shade", "Even_Skaranger_Art", "http://evenskarangerart.deviantart.com/", R.drawable.image04);
        artworks[5] = new Artwork("Ruins_from_the_Past", "Even_Skaranger_Art", "http://evenskarangerart.deviantart.com/", R.drawable.image05);
        //
        artworks[6] = new Artwork("Lifespawn", "Ryuka", "http://ryuka-hime.deviantart.com/", R.drawable.image06);
        artworks[7] = new Artwork("Animal_Heroes ", "Ryuka", "http://ryuka-hime.deviantart.com/", R.drawable.image07);
        artworks[8] = new Artwork("Summertime_is_waterfight_time! ", "Ryuka", "http://ryuka-hime.deviantart.com/", R.drawable.image08);
        artworks[9] = new Artwork("Ladylike Gun", "Ryuka", "http://ryuka-hime.deviantart.com/", R.drawable.image09);
        artworks[10] = new Artwork("Memories", "Ryuka", "http://ryuka-hime.deviantart.com/", R.drawable.image10);
        //
        artworks[11] = new Artwork("The_Trio", "scrson", "http://scrson.deviantart.com/", R.drawable.image11);
        //
        artworks[12] = new Artwork("Kitsunes_Camera", "Miss_Creativity96", "http://miss-creativity96.deviantart.com/", R.drawable.image12);
        artworks[13] = new Artwork("Nyan_Kitsune", "Miss_Creativity96", "http://miss-creativity96.deviantart.com/", R.drawable.image13);
        artworks[14] = new Artwork("Happy_Pinkie_Pie", "Miss_Creativity96", "http://miss-creativity96.deviantart.com/", R.drawable.image14);
        //
        artworks[15] = new Artwork("Yuichiro_Hyakuya", "OtakuSaz", "http://otakusaz.deviantart.com/", R.drawable.image15);
        artworks[16] = new Artwork("Nanako_Dojima", "OtakuSaz", "http://otakusaz.deviantart.com/", R.drawable.image16);
        //
        artworks[17] = new Artwork("Ail_and_An_Sailor_Moon_R", "Nuciferyne", "http://nuciferyne.deviantart.com/", R.drawable.image17);
        artworks[18] = new Artwork("Gossip_on_the_Beach", "Nuciferyne", "http://nuciferyne.deviantart.com/", R.drawable.image18);
        //
        artworks[19] = new Artwork("Lilia", "pin100", "http://pin100.deviantart.com/", R.drawable.image19);
        artworks[20] = new Artwork("Abel_Nightroad", "pin100", "http://pin100.deviantart.com/", R.drawable.image20);
        artworks[21] = new Artwork("Yuki", "pin100", "http://pin100.deviantart.com/", R.drawable.image21);
        //
        artworks[22] = new Artwork("Van_Fanel", "Dennis_Menese", "http://clearmirror-stillh2o.deviantart.com/", R.drawable.image22);
        artworks[23] = new Artwork("KIRA_and_ZERO_Men_of_Justice", "Dennis_Menese", "http://clearmirror-stillh2o.deviantart.com/", R.drawable.image23);
        artworks[24] = new Artwork("World_Martial_Arts_Tournament", "Dennis_Menese", "http://clearmirror-stillh2o.deviantart.com/", R.drawable.image24);
        //
        artworks[25] = new Artwork("Food_Wars", "Shumi", "http://shumijin.deviantart.com/", R.drawable.image25);
        artworks[26] = new Artwork("Haikyuu", "Shumi", "http://shumijin.deviantart.com/", R.drawable.image26);
        artworks[27] = new Artwork("Tokyo_Ghoul", "Shumi", "http://shumijin.deviantart.com/", R.drawable.image27);
        artworks[28] = new Artwork("D_Gray_Man", "Shumi", "http://shumijin.deviantart.com/", R.drawable.image28);
        //
        artworks[29] = new Artwork("Cancer", "eschellan", "http://eschellan.deviantart.com/", R.drawable.image29);
        artworks[30] = new Artwork("Len", "eschellan", "http://eschellan.deviantart.com/", R.drawable.image30);
        //
        artworks[31] = new Artwork("Obon_Night_Free!)", "megumonster", "http://megumonster.deviantart.com/", R.drawable.image31);
        artworks[32] = new Artwork("It_s_a_Day_Off_Kekkai_Sensen)", "megumonster", "http://megumonster.deviantart.com/", R.drawable.image32);
        artworks[33] = new Artwork("My_Wakagashira_Kuroko_no_Basuke)", "megumonster", "http://megumonster.deviantart.com/", R.drawable.image33);
        artworks[34] = new Artwork("In_the_rain", "megumonster", "http://megumonster.deviantart.com/", R.drawable.image34);
        artworks[35] = new Artwork("Courage_Test_crossover", "megumonster", "http://megumonster.deviantart.com/", R.drawable.image35);
        //
        artworks[36] = new Artwork("The_Rain_Lady", "yammybonbon", "http://yammybonbon.deviantart.com/", R.drawable.image36);
        artworks[37] = new Artwork("Sinon", "yammybonbon", "http://yammybonbon.deviantart.com/", R.drawable.image37);
        artworks[38] = new Artwork("The Wizard Howl", "yammybonbon", "http://yammybonbon.deviantart.com/", R.drawable.image38);
        //
        artworks[39] = new Artwork("Kago_to_Torii", "Nuei", "http://nuei.deviantart.com/", R.drawable.image39);
        artworks[40] = new Artwork("Red_Blossom", "Nuei", "http://nuei.deviantart.com/", R.drawable.image40);
        //
        artworks[41] = new Artwork("Sinon", "fuanteinaa", "http://fuanteinaa.deviantart.com/", R.drawable.image41);
        artworks[42] = new Artwork("wonderful", "fuanteinaa", "http://fuanteinaa.deviantart.com/", R.drawable.image42);
        //
        artworks[43] = new Artwork("Ignorance_is_bliss", "Cora_Rm", "http://cora-rosemountain.deviantart.com/", R.drawable.image43);
        artworks[44] = new Artwork("The_best_decoy", "Cora_Rm", "http://cora-rosemountain.deviantart.com/", R.drawable.image44);
        //
        artworks[45] = new Artwork("Madoka_x_Himari", "Eri_chan_45", "http://eri-chan45.deviantart.com/", R.drawable.image45);
        artworks[46] = new Artwork("Shigatsu_wa_kimi_no_uso", "Eri_chan_45", "http://eri-chan45.deviantart.com/", R.drawable.image46);
        artworks[47] = new Artwork("The_sky_at_worlds_end", "Eri_chan_45", "http://eri-chan45.deviantart.com/", R.drawable.image47);
        //
        artworks[48] = new Artwork("Illusion", "Rumi_Kuu", "http://rumi-kuu.deviantart.com", R.drawable.image48);
        artworks[49] = new Artwork("Come_Along_Our_Journey", "Rumi_Kuu", "http://rumi-kuu.deviantart.com", R.drawable.image49);
        artworks[50] = new Artwork("Genesis", "Rumi_Kuu", "http://rumi-kuu.deviantart.com", R.drawable.image50);
        //
        artworks[51] = new Artwork("Heavens_wheel", "Dakiarts", "http://dakiarts.deviantart.com", R.drawable.image51);
        artworks[52] = new Artwork("Flying_freedom ", "Dakiarts", "http://dakiarts.deviantart.com", R.drawable.image52);
        artworks[53] = new Artwork("Fairy_Pond", "Dakiarts", "http://dakiarts.deviantart.com", R.drawable.image53);
        //
        artworks[54] = new Artwork("Cardcaptor_Sakura", "mukimillia", "http://mukimillia.deviantart.com", R.drawable.image54);
        artworks[55] = new Artwork("Amanecer", "mukimillia", "http://mukimillia.deviantart.com", R.drawable.image55);
        artworks[56] = new Artwork("Youre_a_kid_youre_a_squid", "mukimillia", "http://mukimillia.deviantart.com", R.drawable.image56);
        // Dane Celestia
        artworks[57] = new Artwork("Centerstage_1_Elizabeth", "Dane_Celestia", "http://mukimillia.deviantart.com", R.drawable.image57);
        //
        artworks[58] = new Artwork("Kill_La_Kill", "youmistsukisaya", "http://youmistsukisaya.deviantart.com/", R.drawable.image58);
        artworks[59] = new Artwork("Reiner_Rubin", "youmistsukisaya", "http://youmistsukisaya.deviantart.com/", R.drawable.image62);
        artworks[60] = new Artwork("Schnee_Kristall", "youmistsukisaya", "http://youmistsukisaya.deviantart.com/", R.drawable.image60);
        artworks[61] = new Artwork("water_lily", "youmistsukisaya", "http://youmistsukisaya.deviantart.com/", R.drawable.image62);
        //
        artworks[62] = new Artwork("yangyangXD_s_Foxy", "Ailurophile-Chan", "http://youmistsukisaya.deviantart.com/", R.drawable.image62);
        //
        artworks[63] = new Artwork("Mikasa", "Nick-Ian", "http://nick-ian.deviantart.com", R.drawable.image63);
        artworks[64] = new Artwork("Psycho_Pass", "Nick-Ian", "http://nick-ian.deviantart.com", R.drawable.image64);
        artworks[65] = new Artwork("Dragon_Force_Wendy", "Nick-Ian", "http://nick-ian.deviantart.com", R.drawable.image65);
        artworks[66] = new Artwork("Titans", "Nick-Ian", "http://nick-ian.deviantart.com", R.drawable.image66);
        artworks[67] = new Artwork("Shinha", "Nick-Ian", "http://nick-ian.deviantart.com", R.drawable.image67);
        //
        artworks[68] = new Artwork("Bottle Miku", "Dakiarts", "http://dakiarts.deviantart.com", R.drawable.image68);
        //
        artworks[69] = new Artwork("Kisaragi_Momo", "dimdimensions", "http://dimdimensions.deviantart.com", R.drawable.image69);
        artworks[70] = new Artwork("No_Game_No_Life_Shiro", "dimdimensions", "http://dimdimensions.deviantart.com", R.drawable.image70);
        artworks[71] = new Artwork("My_Boyfriend_Is_A_Star", "dimdimensions", "http://dimdimensions.deviantart.com", R.drawable.image71);
        //
        artworks[72] = new Artwork("Onward", "the10s", "http://the10s.deviantart.com", R.drawable.image72);
        artworks[73] = new Artwork("All_Aboard", "the10s", "http://the10s.deviantart.com", R.drawable.image73);
        artworks[74] = new Artwork("From_up_on_poppy_hill", "the10s", "http://the10s.deviantart.com", R.drawable.image74);
        //
        artworks[75] = new Artwork("Marie_Skull_girls", "MurLovely", "http://murlovely.deviantart.com", R.drawable.image75);
        artworks[76] = new Artwork("Cyan_Show_by_Rock", "MurLovely", "http://murlovely.deviantart.com", R.drawable.image76);
        artworks[77] = new Artwork("Chuchu_Show_by_Rock", "MurLovely", "http://murlovely.deviantart.com", R.drawable.image77);
        //
        artworks[78] = new Artwork("nanami_maid", "MurLovely", "http://murlovely.deviantart.com", R.drawable.image78);
        artworks[79] = new Artwork("a_cute_bikini_ikaros", "MurLovely", "http://murlovely.deviantart.com", R.drawable.image79);
        //
        artworks[80] = new Artwork("five_nights_at_freddys", "majesticfuckingeagle", "http://majesticfuckingeagle.deviantart.com", R.drawable.image80);
        //
        artworks[81] = new Artwork("untitled", "Kay", "http://ekaterina-sudsukie.deviantart.com/", R.drawable.image81);
        artworks[82] = new Artwork("owari_no_seraph", "Kay", "http://ekaterina-sudsukie.deviantart.com/", R.drawable.image82);
        artworks[83] = new Artwork("fairy_tale", "Kay", "http://ekaterina-sudsukie.deviantart.com/", R.drawable.image83);
        //
        artworks[84] = new Artwork("Hina_Fukami_Glasslip", "hsm64", "http://hsm64.deviantart.com/", R.drawable.image84);
        artworks[85] = new Artwork("Kill_them_all", "hsm64", "http://hsm64.deviantart.com/", R.drawable.image85);
        artworks[86] = new Artwork("Spike", "hsm64", "http://hsm64.deviantart.com/", R.drawable.image86);
        //
        artworks[87] = new Artwork("Makise_Kurisu", "Dravici_Andrei", "http://draviciandrei.deviantart.com/", R.drawable.image87);
        artworks[88] = new Artwork("Kaori_Miyazono", "Dravici_Andrei", "http://draviciandrei.deviantart.com/", R.drawable.image88);
        artworks[89] = new Artwork("Tetsuya_Kutoko", "Dravici_Andrei", "http://draviciandrei.deviantart.com/", R.drawable.image89);
        artworks[90] = new Artwork("Saber_Lily", "Dravici_Andrei", "http://draviciandrei.deviantart.com/", R.drawable.image90);
        //
        artworks[91] = new Artwork("Kosaki_Onodera_Nisekoi", "Starsilvery", "http://starsilvery.deviantart.com/", R.drawable.image91);
        artworks[92] = new Artwork("Chitoge_Kirisaki_Nisekoi", "Starsilvery", "http://starsilvery.deviantart.com/", R.drawable.image92);
        //
        artworks[93] = new Artwork("elsa", "JESSE", "http://yiichan.deviantart.com/", R.drawable.image93);
        artworks[94] = new Artwork("missfortune", "JESSE", "http://yiichan.deviantart.com/", R.drawable.image94);
        //
        artworks[95] = new Artwork("Chibi_Hestia", "Spaca_Scaleno", "http://spaca.deviantart.com", R.drawable.image95);
        artworks[96] = new Artwork("Magic_Girls_Elesisand_Edel", "Spaca_Scaleno", "http://spaca.deviantart.com", R.drawable.image96);
        //
        artworks[97] = new Artwork("manaka", "Emi_Liu", "http://emi-liu.deviantart.com", R.drawable.image97);
        artworks[98] = new Artwork("bottle_miku", "Emi_Liu", "http://emi-liu.deviantart.com", R.drawable.image98);
        artworks[99] = new Artwork("junko_enoshima", "Emi_Liu", "http://emi-liu.deviantart.com", R.drawable.image99);
        artworks[100] = new Artwork("maki", "Emi_Liu", "http://emi-liu.deviantart.com", R.drawable.image100);
        //
        artworks[101] = new Artwork("Kiki", "jubilri", "http://jubilri.deviantart.com/", R.drawable.image101);
        artworks[102] = new Artwork("Homura", "jubilri", "http://jubilri.deviantart.com/", R.drawable.image102);
        //
        artworks[103] = new Artwork("Thunder", "DistructiveMusic", "http://distructivemusic.deviantart.com/", R.drawable.image103);
        artworks[104] = new Artwork("Titan", "DistructiveMusic", "http://distructivemusic.deviantart.com/", R.drawable.image104);
        //
        artworks[105] = new Artwork("Hanayamata", "Artemisumi", "http://distructivemusic.deviantart.com/", R.drawable.image105);
        artworks[106] = new Artwork("Rachel_vs_Morrigan", "Artemisumi", "http://distructivemusic.deviantart.com/", R.drawable.image106);
        //
        artworks[107] = new Artwork("miyoko", "AkaneYurika", "http://akaneyurika.deviantart.com", R.drawable.image107);
        artworks[108] = new Artwork("sayanara_miyoko", "AkaneYurika", "http://akaneyurika.deviantart.com", R.drawable.image108);
        artworks[109] = new Artwork("awatar", "AkaneYurika", "http://akaneyurika.deviantart.com", R.drawable.image109);
        //
        artworks[110] = new Artwork("Cyber_Ene", "Cloudkourin", "http://cloudkourin.deviantart.com/", R.drawable.image110);
        artworks[111] = new Artwork("No_Game_No_Life", "Cloudkourin", "http://cloudkourin.deviantart.com/", R.drawable.image111);
        artworks[112] = new Artwork("Childhood_s_End", "Cloudkourin", "http://cloudkourin.deviantart.com/", R.drawable.image112);
        //
        artworks[113] = new Artwork("Jafar", "Rikuross", "http://rikuross.deviantart.com/", R.drawable.image113);
        //
        artworks[114] = new Artwork("Our_Home", "Gavryll", "http://gavryll.deviantart.com/", R.drawable.image114);
        //
        artworks[115] = new Artwork("EcaxDigimon", "Ragha_Rakita", "http://ragha-rakita.deviantart.com/", R.drawable.image115);
        artworks[116] = new Artwork("Chitoge-Nisekoi", "Ragha_Rakita", "http://ragha-rakita.deviantart.com/", R.drawable.image116);
        artworks[117] = new Artwork("Flaga_Haimura_Moroha", "Ragha_Rakita", "http://ragha-rakita.deviantart.com/", R.drawable.image117);
        artworks[118] = new Artwork("Isla_Plastic_Memories", "Ragha_Rakita", "http://ragha-rakita.deviantart.com/", R.drawable.image118);
        //
        artworks[119] = new Artwork("chibi_miku", "mikuandmord", "http://mikuandmord.deviantart.com/", R.drawable.image119);
        artworks[120] = new Artwork("black_rock_shooter", "mikuandmord", "http://mikuandmord.deviantart.com/", R.drawable.image120);
        //
        artworks[121] = new Artwork("untitled", "famyuchii", "http://famyuchii.deviantart.com/", R.drawable.image121);
        //
        artworks[122] = new Artwork("Star_Butterfly_Rainbow_Splash", "AyaHimeTenshi", "http://ayahimetenshi.deviantart.com/", R.drawable.image122);
        //
        artworks[123] = new Artwork("Smile", "Misutore01", "http://misutore01.deviantart.com/", R.drawable.image123);
        //
        artworks[124] = new Artwork("Minato112", "Misutore01", "http://misutore01.deviantart.com/", R.drawable.image124);
        //
        artworks[125] = new Artwork("TORI_Whats_The_Matter_Pikachu", "GreenIbr", "http://misutore01.deviantart.com/", R.drawable.image125);
        //
        artworks[126] = new Artwork("if_i_were_20_years_younger_i_d_wear_you", "CandideKun", "http://candide1337.deviantart.com/", R.drawable.image126);
        //
        artworks[127] = new Artwork("Nami", "Rozemira", "http://rozemira.deviantart.com/", R.drawable.image127);
        artworks[128] = new Artwork("Fun_and_Spells_with_Lux", "Rozemira", "http://rozemira.deviantart.com/", R.drawable.image128);
        //
        artworks[129] = new Artwork("Atelier Meruru", "Yoneyu", "http://yoneyu.deviantart.com/", R.drawable.image129);
        //
        artworks[130] = new Artwork("Kanato", " BonBonPoupee", " https://www.facebook.com/BonBonPoupeeArt", R.drawable.image130);
        artworks[131] = new Artwork("Dreaming Mary", " BonBonPoupee", " https://www.facebook.com/BonBonPoupeeArt", R.drawable.image131);
        artworks[132] = new Artwork("Aya", " BonBonPoupee", " https://www.facebook.com/BonBonPoupeeArt", R.drawable.image132);
        //
        artworks[133] = new Artwork("Yumeo Ochida", "M.Milles.Mark", "http://hananueaoko.deviantart.com/", R.drawable.image133);
        //
        artworks[134] = new Artwork("Slayer girl", "Lora", "http://omikuy.deviantart.com/", R.drawable.image134);
        artworks[135] = new Artwork("Radiant Spirit", "Destiny Sword", "http://destinysword.deviantart.com/", R.drawable.image135);
        artworks[136] = new Artwork("Open Heart", "Destiny Sword", "http://destinysword.deviantart.com/", R.drawable.image136);
        artworks[137] = new Artwork("AKB0048 Makoto Yokomizo", "PikaSetsuna", "http://pikasetsuna.deviantart.com/", R.drawable.image137);
        //
        artworks[138] = new Artwork("Badass Girl", "Ferensa", "http://the-sweetshop-fc.deviantart.com/", R.drawable.image138);
        //
        artworks[139] = new Artwork("Angel", "Shimozuku", "http://shimozuku.deviantart.com/", R.drawable.image139);
        //
        artworks[140] = new Artwork("Magical", "BluePikachuSenpai", "http://bluepikachusenpai.deviantart.com/", R.drawable.image140);

        artworks[141] = new Artwork("yui k_on", "Nikkon Heguy", "http://nikkonh.deviantart.com/", R.drawable.image141);
    }
}